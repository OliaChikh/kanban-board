// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAT6iOh4nOFijNTLg2H9IP7LGCFHFq28tM',
    authDomain: 'kanban-board-fe696.firebaseapp.com',
    projectId: 'kanban-board-fe696',
    storageBucket: 'kanban-board-fe696.appspot.com',
    messagingSenderId: '650245644226',
    appId: '1:650245644226:web:b532932532b92c2969405b',
    measurementId: 'G-BY6JQN7KVJ'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
