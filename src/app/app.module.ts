import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {ContenteditableModule} from '@ng-stack/contenteditable';

import { AppComponent } from './app.component';
import { ColumnComponent } from './components/column/column.component';
import { BoardComponent } from './components/board/board.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from '../environments/environment';
import { TaskComponent } from './components/task/task.component';
import { ColumnService } from './servises/column.service';
import { AddColumnComponent } from './components/add-column/add-column.component';
import { AddTaskComponent } from './components/add-task/add-task.component';

@NgModule({
  declarations: [
    AppComponent,
    ColumnComponent,
    BoardComponent,
    TaskComponent,
    AddColumnComponent,
    AddTaskComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    DragDropModule,
    ContenteditableModule,
    AngularFireModule.initializeApp(environment.firebase, 'angularfs'),
    AngularFirestoreModule
  ],
  providers: [ColumnService],
  bootstrap: [AppComponent]
})
export class AppModule { }
