export interface Column {
  nameColumn?: string;
  id?: string;
  createdDate: number;
}
