export interface Task {
  columnId: string;
  text: string;
  numberOfLike: number;
  listOfComments: string[];
  id?: string;
  relativeOrder: number;
}
