import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import {
  AngularFirestore,
  AngularFirestoreCollection
} from 'angularfire2/firestore';
import { Task } from 'src/app/model/task.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  taskCollection: AngularFirestoreCollection<Task>;

  constructor(public afs: AngularFirestore) {
    this.taskCollection = this.afs.collection('tasks');
  }

  getTasks(columnId: string): Observable<Task[]> {
    return this.taskCollection.snapshotChanges().pipe(
      map((changes) => {
        return changes
          .map((a) => {
            const data = a.payload.doc.data() as Task;
            data.id = a.payload.doc.id;
            return data;
          })
          .filter((task) => task.columnId === columnId)
          .sort((a: Task, b: Task) => a.relativeOrder - b.relativeOrder);
      })
    );
  }

  addTask(task: Task): void {
    task.relativeOrder = new Date().getTime();
    this.taskCollection.add(task);
  }

  updateTask(task: Task): void {
    this.taskCollection.doc(task.id).update({ ...task });
  }

  deleteTask(id: string): void {
    this.taskCollection.doc(id).delete();
  }
}
