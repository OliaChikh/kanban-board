import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import {
  AngularFirestore,
  AngularFirestoreCollection
} from 'angularfire2/firestore';
import { Column } from 'src/app/model/column.model';

@Injectable({
  providedIn: 'root',
})
export class ColumnService {
  columnCollection: AngularFirestoreCollection<Column>;
  columns: Observable<Column[]>;

  constructor(public afs: AngularFirestore) {
    this.columnCollection = this.afs.collection('columns');
    this.columns = this.columnCollection.snapshotChanges().pipe(
      map((changes) => {
        return changes
          .map((a) => {
            const data = a.payload.doc.data() as Column;
            data.id = a.payload.doc.id;
            return data;
          })
          .sort((a: Column, b: Column) => {
            return a.createdDate - b.createdDate;
          });
      })
    );
  }

  getColumns(): Observable<Column[]> {
    return this.columns;
  }

  addColumn(column: Column): void {
    this.columnCollection.add(column);
  }

  deleteColumn(id: string): void {
    this.columnCollection.doc(id).delete();
  }
}
