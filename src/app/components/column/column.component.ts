import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { EventEmitter, Input, Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/servises/task.service';
import { Task } from '../../model/task.model';

@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.scss'],
})
export class ColumnComponent implements OnInit {
  @Input() id: string;
  @Input() columnName: string;
  @Input() otherNames: string;
  @Output() removeColumn: EventEmitter<number> = new EventEmitter();

  listOfTasks: Task[] = [];
  isShown: boolean;
  taskComment: string;

  constructor(private taskService: TaskService) {}

  ngOnInit(): void {
    this.taskService.getTasks(this.id).subscribe((tasks) => {
      this.listOfTasks = tasks;
    });

    this.isShown = false;
  }

  deleteColumn(): void {
    this.listOfTasks.forEach((task) => this.taskService.deleteTask(task.id));
    this.removeColumn.emit();
  }

  drop(event: CdkDragDrop<Task[]>): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      for (let i = 0; i < this.listOfTasks.length; i++) {
        this.listOfTasks[i].relativeOrder = i;
        this.taskService.updateTask(this.listOfTasks[i]);
      }
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      for (let i = 0; i < event.container.data.length; i++) {
        event.container.data[i].columnId = event.container.id;
        event.container.data[i].relativeOrder = i;
        this.taskService.updateTask(event.container.data[i]);
      }
    }
  }

  createTask(): void {
    this.isShown = true;
  }

  closeModalAddTask(): void {
    this.isShown = false;
  }

  addTask(): void {
    this.isShown = false;
  }

  removeTask(index: number): void {
    this.taskService.deleteTask(this.listOfTasks[index].id);
  }

  showComment(id: string): void {
    if (id === this.taskComment) {
      this.taskComment = undefined;
    } else {
      this.taskComment = id;
    }
  }
}
