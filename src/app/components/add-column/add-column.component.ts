import { Component, Output } from '@angular/core';
import { ColumnService } from 'src/app/servises/column.service';
import { Column } from '../../model/column.model';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-column',
  templateUrl: './add-column.component.html',
  styleUrls: ['./add-column.component.scss'],
})
export class AddColumnComponent {
  @Output() closeModalAddColumn: EventEmitter<boolean> = new EventEmitter();
  @Output() addColumn: EventEmitter<boolean> = new EventEmitter();

  column: Column = {
    nameColumn: '',
    createdDate: new Date().getTime(),
  };

  constructor(private columnService: ColumnService) {}

  addColumnToServer(): void {
    if (this.column.nameColumn === '') {
      return;
    }
    this.column.createdDate = new Date().getTime();
    this.columnService.addColumn(this.column);
    this.column.nameColumn = '';
    this.addColumn.emit();
  }

  closeModal(): void {
    this.closeModalAddColumn.emit();
  }
}
