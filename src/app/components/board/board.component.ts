import { Component, OnInit } from '@angular/core';
import { ColumnService } from 'src/app/servises/column.service';
import { TaskService } from 'src/app/servises/task.service';
import { Column } from '../../model/column.model';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {
  columns: Column[];
  isShown: boolean;
  otherNames: string;
  inputColumn = '';

  constructor(
    private columnService: ColumnService,
    private taskService: TaskService
  ) {}

  ngOnInit(): void {
    this.columnService.getColumns().subscribe((columns) => {
      this.columns = columns;
    });
  }

  removeColumn(id: number): void {
    this.columnService.deleteColumn(this.columns[id].id);
  }

  createColumn(): void {
    this.isShown = true;
  }

  closeModalAddColumn(): void {
    this.isShown = false;
  }

  addColumn(): void {
    this.isShown = false;
  }

  getOtherNames(currentName: string): string {
    return `[${this.columns
      .filter((column) => column.nameColumn !== currentName)
      .map((column) => column.nameColumn)
      .toString()}]`;
  }
}
