import { Component, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { TaskService } from 'src/app/servises/task.service';
import { Task } from '../../model/task.model';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
})
export class TaskComponent {
  @Input() id: string;
  @Input() columnId: string;
  @Input() text: string;
  @Input() numberOfLike: number;
  @Input() showListOfComments: boolean;
  @Input() listOfComments: string[];
  @Input() relativeOrder: number;
  @Output() removeTask: EventEmitter<number> = new EventEmitter();
  @Output() showComment: EventEmitter<number> = new EventEmitter();
  @Output() addComment: EventEmitter<number> = new EventEmitter();

  inputComment = '';

  constructor(private taskService: TaskService) {}

  deleteTask(): void {
    this.removeTask.emit();
  }

  clickComment(): void {
    this.showComment.emit();
  }

  getTask(): Task {
    return {
      id: this.id,
      columnId: this.columnId,
      text: this.text,
      numberOfLike: this.numberOfLike,
      listOfComments: this.listOfComments,
      relativeOrder: this.relativeOrder,
    };
  }

  clickLike(): void {
    this.numberOfLike++;
    this.taskService.updateTask(this.getTask());
  }

  clickAddComment(): void {
    this.listOfComments.push(this.inputComment);
    this.inputComment = '';
    this.taskService.updateTask(this.getTask());
  }
}
