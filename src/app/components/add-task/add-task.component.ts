import { Component, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { TaskService } from 'src/app/servises/task.service';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss'],
})
export class AddTaskComponent {
  @Input() columnId: string;
  @Output() closeModalAddTask: EventEmitter<boolean> = new EventEmitter();
  @Output() addTask: EventEmitter<boolean> = new EventEmitter();

  inputTask = '';

  constructor(private taskService: TaskService) {}

  addTaskToServer(): void {
    if (this.inputTask === '') {
      return;
    }
    const task = {
      columnId: this.columnId,
      text: this.inputTask,
      numberOfLike: null,
      listOfComments: [],
      relativeOrder: 0,
    };
    this.taskService.addTask(task);
    this.addTask.emit();
  }

  closeModal(): void {
    this.closeModalAddTask.emit();
  }

  enterSubmit(event): void{
    if (event.target.value === '') {
      event.preventDefault();
    } else {
      this.addTaskToServer();
    }
 }
}
